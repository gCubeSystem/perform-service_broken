package org.gcube.application.perform.service.engine.model;

public class InternalException extends Exception {

	public InternalException() {
		// TODO Auto-generated constructor stub
	}

	public InternalException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InternalException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InternalException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InternalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
