package org.gcube.application.perform.service.engine.dm;

public class DMException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3390718985211520032L;

	public DMException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DMException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public DMException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DMException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DMException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
